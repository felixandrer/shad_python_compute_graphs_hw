from collections import Counter
from graph.compute_graph import *
from graph.graph_operations import *
from math import sin, cos, sqrt, atan2, radians
from datetime import datetime
import re


def count_travel_time_mapper(record):
    leave_time = record['leave_time']
    enter_time = record['enter_time']

    if len(leave_time) > 15:
        FORMAT1 = '%Y%m%dT%H%M%S.%f'
    else:
        FORMAT1 = '%Y%m%dT%H%M%S'
    if len(enter_time) > 15:
        FORMAT2 = '%Y%m%dT%H%M%S.%f'
    else:
        FORMAT2 = '%Y%m%dT%H%M%S'
    leave_time = datetime.strptime(leave_time, FORMAT1)
    enter_time = datetime.strptime(enter_time, FORMAT2)
    tdelta = leave_time - enter_time
    travel_time = tdelta.total_seconds() / (60 * 60)
    yield {
        'edge_id': record['edge_id'],
        'travel_time': travel_time,
        'weekday': enter_time.weekday(),
        'hour': enter_time.hour
    }


def count_len_mapper(record):
    R = 6373.0

    lat1 = radians(record['start'][1])
    lon1 = radians(record['start'][0])
    lat2 = radians(record['end'][1])
    lon2 = radians(record['end'][0])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    yield {
        'edge_id': record['edge_id'],
        'len': distance
    }


def count_speed_mapper(record):
    yield {
        'edge_id': record['edge_id'],
        'speed': record['len'] / record['travel_time'],
        'weekday': record['weekday'],
        'hour': record['hour']
    }


def count_avrspeed_reducer(records):
    average = sum(record['speed'] for record in records) / len(records)
    yield {
        'weekday': records[0]['weekday'],
        'hour': records[0]['hour'],
        'avrspeed': average
    }


if __name__ == '__main__':
    graph_data = ComputeGraph()
    graph_data.add_operation(Input('graph_data.txt'))
    graph_data.add_operation(Map(count_len_mapper))

    travel_times = ComputeGraph()
    travel_times.add_operation(Input('travel_times.txt'))
    travel_times.add_operation(Map(count_travel_time_mapper))
    travel_times.add_operation(Join(graph_data, ['edge_id'], strategy='inner'))
    travel_times.add_operation(Map(count_speed_mapper))
    travel_times.add_operation(Reduce(count_avrspeed_reducer,
                               ['weekday', 'hour']))
    travel_times.run('example4_answer.txt')
