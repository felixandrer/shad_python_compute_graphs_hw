from collections import Counter
from graph.compute_graph import *
from graph.graph_operations import *
from math import log
import re


def tokenizer_mapper(record):
    only_letters = re.compile('[^\d_]+')
    for match in re.compile('\w+').finditer(record['text']):
        if only_letters.fullmatch(match[0]) is not None:
            if len(match[0]) > 4:
                yield {
                    'word': match[0].lower(),
                    'doc_id': record['doc_id']
                }


def word_conter_in_doc_reducer(records):
    yield {
        'doc_id': records[0]['doc_id'],
        'word': records[0]['word'],
        'word_count_in_doc': len(records)
    }


def word_conter_in_all_docs_reducer(records):
    total = sum(record['word_count_in_doc'] for record in records)
    for record in records:
        yield {**record, 'word_count_in_all_docs': total}


def doc_count_folder(state, new_string):
    return {'total_num_of_docs': state['total_num_of_docs'] + 1}


def del_rare_words_reducer(records):
    to_del = False
    for record in records:
        if record['word_count_in_doc'] < 2:
            to_del = True
    if not(to_del) and len(records) == records[0]['total_num_of_docs']:
        for record in records:
            yield record


def count_pmi_mapper(record):
    yield {
        'doc_id': record['doc_id'],
        'word': record['word'],
        'pmi': log(record['word_count_in_doc'] /
                   record['word_count_in_all_docs'])
    }


def leave_top10_words_reducer(records):
    records.sort(key=lambda x: x['pmi'], reverse=True)
    top10 = [record['word'] for record in records[:10]]
    yield {
        'doc_id': records[0]['doc_id'],
        'top10': top10
    }


if __name__ == '__main__':
    docs_count = ComputeGraph()
    docs_count.add_operation(Input('text_corpus.txt'))
    docs_count.add_operation(Fold(doc_count_folder, {'total_num_of_docs': 0}))

    graph = ComputeGraph()
    graph.add_operation(Input('text_corpus.txt'))
    graph.add_operation(Map(tokenizer_mapper))
    graph.add_operation(Reduce(word_conter_in_doc_reducer, ['doc_id', 'word']))
    graph.add_operation(Join(docs_count, strategy='outer'))
    graph.add_operation(Reduce(del_rare_words_reducer, ['word']))
    graph.add_operation(Reduce(word_conter_in_all_docs_reducer, ['word']))
    graph.add_operation(Map(count_pmi_mapper))
    graph.add_operation(Reduce(leave_top10_words_reducer, ['doc_id']))
    graph.run('example3_answer.txt')
