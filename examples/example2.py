from collections import Counter
from graph.compute_graph import *
from graph.graph_operations import *
from math import log
import re


def term_frequency_reducer(records):
    word_count = Counter()
    for r in records:
        word_count[r['word']] += 1
    total = sum(word_count.values())
    for w, count in word_count.items():
        yield {
            'doc_id': r['doc_id'],
            'word': w,
            'tf': count / total
        }


def tokenizer_mapper(record):
    only_letters = re.compile('[^\d_]+')
    for match in re.compile('\w+').finditer(record['text']):
        if only_letters.fullmatch(match[0]) is not None:
            yield {
                'word': match[0].lower(),
                'doc_id': record['doc_id']
                  }


def multiply_tf_and_idf_mapper(row):
    yield {
        'word': row['word'],
        'doc_id': row['doc_id'],
        'tf_idf': row['tf'] * row['idf']
    }


def leave_top3_docs_reducer(records):
    records.sort(key=lambda x: x['tf_idf'], reverse=True)
    top3 = [[record['doc_id'], record['tf_idf']] for record in records[:3]]
    yield {
        'word': records[0]['word'],
        'index': top3
    }


def count_idf_mapper(record):
    idf = log(record['total_num_of_docs']/record['num_of_docs_for_word'])
    yield {
        'word': record['word'],
        'idf': idf
    }


def doc_counter_reducer(records):
    yield {
        'word': records[0]['word'],
        'total_num_of_docs': records[0]['total_num_of_docs'],
        'num_of_docs_for_word': len(records)
    }


def doc_count_folder(state, new_string):
    return {'total_num_of_docs': state['total_num_of_docs'] + 1}


def leave_only_one_reducer(records):
    yield records[0]


if __name__ == '__main__':
    split_words = ComputeGraph()
    split_words.add_operation(Input('text_corpus.txt'))
    split_words.add_operation(Map(tokenizer_mapper))

    docs_count = ComputeGraph()
    docs_count.add_operation(Input('text_corpus.txt'))
    docs_count.add_operation(Fold(doc_count_folder, {'total_num_of_docs': 0}))

    count_idf = ComputeGraph()
    count_idf.add_operation(Input(split_words))
    count_idf.add_operation(Reduce(leave_only_one_reducer, ['doc_id', 'word']))
    count_idf.add_operation(Join(docs_count, strategy='outer'))
    count_idf.add_operation(Reduce(doc_counter_reducer, ['word']))
    count_idf.add_operation(Map(count_idf_mapper))

    calc_index = ComputeGraph()
    calc_index.add_operation(Input(split_words))
    calc_index.add_operation(Reduce(term_frequency_reducer, ['doc_id']))
    calc_index.add_operation(Join(count_idf, ['word'], strategy='inner'))
    calc_index.add_operation(Map(multiply_tf_and_idf_mapper))
    calc_index.add_operation(Reduce(leave_top3_docs_reducer, ['word']))
    calc_index.run('example2_answer.txt')
