from collections import Counter
from graph.compute_graph import *
from graph.graph_operations import *
import re


def tokenizer_mapper(record):
    only_letters = re.compile('[^\d_]+')
    for match in re.compile('\w+').finditer(record['text']):
        if only_letters.fullmatch(match[0]) is not None:
            yield {
                    'word': match[0].lower(),
                    'doc_id': record['doc_id']
                  }


def word_conter_reducer(records):
    yield {
        'word': records[0]['word'],
        'frequency': len(records)
    }


if __name__ == '__main__':
    graph = ComputeGraph()
    graph.add_operation(Input('text_corpus.txt'))
    graph.add_operation(Map(tokenizer_mapper))
    graph.add_operation(Reduce(word_conter_reducer, ['word']))
    graph.add_operation(Sort(['frequency']))
    graph.run(output_filename='example1_answer.txt')
