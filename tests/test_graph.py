import pytest
import pandas as pd
from graph.compute_graph import *
from graph.graph_operations import *


def test_sort():
    data = [
        {'name': 'Vanya', 'room': 122},
        {'name': 'Ira', 'room': 120},
        {'name': 'Nastya', 'room': 112},
        {'name': 'Vadim', 'room': 123},
        {'name': 'Igor', 'room': 125}
    ]
    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Sort(['name'], reverse=False))
    data.sort(key=lambda x: x['name'], reverse=False)
    assert(data == graph.run())


def test_sort_with_wrong_key():
    data = [
        {'name': 'Vanya', 'room': 122},
        {'name': 'Ira', 'room': 120},
        {'name': 'Nastya', 'room': 112},
        {'name': 'Vadim', 'room': 123},
        {'name': 'Igor', 'room': 125}
    ]
    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Sort(['somekey'], reverse=False))
    with pytest.raises(ValueError):
        graph.run()


def test_map():
    data = [
        {'val': 1, 'power': 1},
        {'val': 2, 'power': 2},
        {'val': 3, 'power': 3},
        {'val': 4, 'power': 4}
    ]

    def mapper(record):
        yield {
            **record,
            'result': record['val'] ** record['power']
        }
    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Map(mapper))
    results = [row['result'] for row in graph.run()]
    assert(results == [1, 4, 27, 256])


def test_multiple_map():
    data = [
        {'id': 1, 'value': 1},
        {'id': 2, 'value': 2},
        {'id': 3, 'value': 3},
        {'id': 4, 'value': 4}
    ]

    def mapper(record):
        for i in range(record['value']):
            yield record
    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Map(mapper))
    result = graph.run()
    result_ids = [row['id'] for row in result]
    assert(result_ids == [1, 2, 2, 3, 3, 3, 4, 4, 4, 4])


def test_fold():
    data = [
        {'id': 1, 'text': 'just_for_test'},
        {'id': 2, 'text': 'just_for_test'},
        {'id': 3, 'text': 'just_for_test'},
        {'id': 4, 'text': 'just_for_test'},
        {'id': 5, 'text': 'just_for_test'},
        {'id': 6, 'text': 'just_for_test'}
    ]

    def count_rows_folder(state, record):
        state['row_counter'] += 1
        return state
    init_state = {'row_counter': 0}
    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Fold(count_rows_folder, init_state))
    assert(graph.run() == [{'row_counter': 6}])


def test_inner_left_right_outer_join():
    for strat in ['inner', 'left', 'right', 'outer']:
        people_data = [
            {'id': 1, 'name': 'Vanya', 'room': 122},
            {'id': 2, 'name': 'Ira', 'room': 120},
            {'id': 3, 'name': 'Nastya', 'room': 112},
            {'id': 4, 'name': 'Vadim', 'room': 112},
            {'id': 5, 'name': 'Igor', 'room': 125}
        ]
        department_data = [
            {'room': 120, 'department': 'JazzMusic'},
            {'room': 121, 'department': 'Cooking'},
            {'room': 122, 'department': 'NetflixAndChill'},
            {'room': 123, 'department': 'ML'},
            {'room': 124, 'department': 'Chess'}
        ]
        department_graph = ComputeGraph()
        department_graph.add_operation(Input(department_data))

        people_graph = ComputeGraph()
        people_graph.add_operation(Input(people_data))
        people_graph.add_operation(Join(department_graph, ['room'],
                                   strategy=strat))
        graph_result = pd.DataFrame(people_graph.run())

        people_data = pd.DataFrame(people_data)
        department_data = pd.DataFrame(department_data)
        true_result = people_data.merge(department_data, on=['room'],
                                        how=strat)

        graph_result = graph_result.sort_values(by=['id'])
        graph_result.reset_index(drop=True, inplace=True)
        graph_result.sort_index(axis=1, inplace=True)
        true_result = true_result.sort_values(by=['id'])
        true_result.reset_index(drop=True, inplace=True)
        true_result.sort_index(axis=1, inplace=True)
        assert(graph_result.equals(true_result))


def test_outer_join_with_no_key():
    people_data = [
        {'id': 1, 'name': 'Vanya', 'room': 122},
        {'id': 2, 'name': 'Ira', 'room': 120},
        {'id': 3, 'name': 'Nastya', 'room': 112},
        {'id': 4, 'name': 'Vadim', 'room': 112},
        {'id': 5, 'name': 'Igor', 'room': 125}
    ]
    work_data = [
        {'working_hours': 8}
    ]
    work_graph = ComputeGraph()
    work_graph.add_operation(Input(work_data))

    people_graph = ComputeGraph()
    people_graph.add_operation(Input(people_data))
    people_graph.add_operation(Join(work_graph, [], strategy='outer'))
    graph_result = people_graph.run()

    true_result = [
        {'id': 1, 'name': 'Vanya', 'room': 122, 'working_hours': 8},
        {'id': 2, 'name': 'Ira', 'room': 120, 'working_hours': 8},
        {'id': 3, 'name': 'Nastya', 'room': 112, 'working_hours': 8},
        {'id': 4, 'name': 'Vadim', 'room': 112, 'working_hours': 8},
        {'id': 5, 'name': 'Igor', 'room': 125, 'working_hours': 8}
    ]
    assert(graph_result == true_result)


def test_join_with_wrong_key():
    people_data = [
        {'id': 1, 'name': 'Vanya', 'room': 122},
        {'id': 2, 'name': 'Ira', 'room': 120},
        {'id': 3, 'name': 'Nastya', 'room': 112}
    ]
    department_data = [
            {'room': 120, 'department': 'JazzMusic'},
            {'room': 121, 'department': 'Cooking'}
    ]
    department_graph = ComputeGraph()
    department_graph.add_operation(Input(department_data))

    people_graph = ComputeGraph()
    people_graph.add_operation(Input(people_data))
    people_graph.add_operation(Join(department_graph, ['id'],
                               strategy='inner'))
    with pytest.raises(ValueError):
        graph_result = people_graph.run()


def test_join_with_key_and_empty_data():
    people_data = [
        {'id': 1, 'name': 'Vanya', 'room': 122},
        {'id': 2, 'name': 'Ira', 'room': 120},
        {'id': 3, 'name': 'Nastya', 'room': 112}
    ]
    department_data = []
    department_graph = ComputeGraph()
    department_graph.add_operation(Input(department_data))

    people_graph = ComputeGraph()
    people_graph.add_operation(Input(people_data))
    people_graph.add_operation(Join(department_graph, ['id'],
                               strategy='inner'))
    with pytest.raises(ValueError):
        graph_result = people_graph.run()


def test_reduce():
    data = [
        {'id': 1},
        {'id': 2},
        {'id': 2},
        {'id': 3},
        {'id': 4},
        {'id': 5},
        {'id': 3},
    ]

    def leave_unique_reducer(records):
        yield records[0]

    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Reduce(leave_unique_reducer, ['id']))
    result_ids = [row['id'] for row in graph.run()]
    assert (result_ids == [1, 2, 3, 4, 5])


def test_reduce_with_wring_key():
    data = [
        {'id': 1},
        {'id': 2},
        {'id': 2}
    ]

    def leave_unique_reducer(records):
        yield records[0]

    graph = ComputeGraph()
    graph.add_operation(Input(data))
    graph.add_operation(Reduce(leave_unique_reducer, ['name']))
    with pytest.raises(ValueError):
        result = graph.run()
