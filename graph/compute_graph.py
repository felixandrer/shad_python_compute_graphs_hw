import json
from graph.graph_operations import *


def dfs(graph, topological_order):
    """
    Return list of graphs sorted in topological order.
    If cycle found, raise ValueError.
    """
    graph.dfs_color = 'gray'
    for subgraph in graph.subgraphs:
        if hasattr(subgraph, 'dfs_color') and subgraph.dfs_color == 'gray':
            raise ValueError('Сycle in computations appeared')
        elif not(hasattr(subgraph, 'dfs_color')):
            dfs(subgraph, topological_order)
    graph.dfs_color = 'black'
    topological_order.append(graph)


def clear_after_dfs(topological_order):
    """
    Should be called after dfs function to delete excess graph's attribute
    «dfs_color» that appeared during dfs.
    """
    for graph in topological_order:
        if hasattr(graph, 'dfs_color'):
            del graph.dfs_color


class ComputeGraph(object):
    """Computational graph class."""
    def __init__(self):
        """
        Graph constructor.
        :table: data that graph will work with.
        :operation_list: list of operation, that will be performed by graph.
        :subgraphs: list of graphs on which this graph depends.
        """
        self.table = []
        self.operation_list = []
        self.subgraphs = []

    def add_operation(self, operation):
        """
        Add operation to self.operation_list.
        Check whether a new dependency on another graph has appeared. If so,
        add graph to the :subgraphs:
        """
        self.operation_list.append(operation)
        if isinstance(operation, (Input, Join)):
            if isinstance(operation.input_object, ComputeGraph):
                self.subgraphs.append(operation.input_object)

    def run_operations(self, verbose=False):
        """
        Run the operations from self.operation_list.
        :verbose: (optional) when True, print the sequence of running
        operations.
        """
        for operation in self.operation_list:
            if verbose:
                print('Current operation: ', operation.__class__.__name__)
            if isinstance(operation, Input):
                self.table = operation.run()
                continue
            operation.data = self.table
            if isinstance(operation, Sort):
                operation.run()
                continue
            if isinstance(operation, Fold):
                new_table = [operation.run()]
            if isinstance(operation, (Map, Reduce, Join)):
                new_table = [string for string in operation.run()]
            self.table = new_table
        return self.table

    def run(self, output_filename=None, verbose=False):
        """
        Run graph and return result as a list of dicts or dump it as JSON to
        output_file
        :output_filename: (optional) name of the file to which you want to
        write the result of the graph computation.
        :verbose: (optional) when True, print the sequence of running
        operations.
        """
        inputs = {}
        topological_order = []
        dfs(self, topological_order)
        clear_after_dfs(topological_order)
        for graph in topological_order:
            if verbose:
                print("New graph in process")
            graph.update_inputs(inputs)
            inputs[graph] = graph.run_operations(verbose)
        if output_filename is not None:
            with open(output_filename, 'w') as f:
                for string in self.table:
                    json.dump(string, f)
                    f.write('\n')
        else:
            return self.table

    def update_inputs(self, inputs):
        for operation in self.operation_list:
            if isinstance(operation, (Join, Input)):
                operation.possible_inputs = inputs
