from copy import deepcopy
from itertools import groupby
from itertools import product
import operator
import json
from graph import compute_graph


class Input(object):
    """
    Input operation. Must be called before any other operations. Determines
    where the graph should take input data.
    """
    def __init__(self, input_object):
        """
        Input operation constructor.
        :input_object: can be str, ComputeGraph or list of dicts.
        :possible_inputs: dict of inputs, that were calculated during previous
        stages.
        """
        self.input_object = input_object
        self.possible_inputs = {}

    def run(self):
        """
        Return data obtained from the input_object.
        """
        if isinstance(self.input_object, str):
            result = []
            with open(self.input_object, 'r') as f:
                for line in f:
                    result.append(json.loads(line))
        elif isinstance(self.input_object, list):
            result = deepcopy(self.input_object)
        elif isinstance(self.input_object, compute_graph.ComputeGraph):
            result = self.possible_inputs[self.input_object]
            self.possible_inputs = {}
        else:
            raise TypeError('Wrong type of input')
        return result


class Map(object):
    """Map operation."""
    def __init__(self, mapper):
        """
        Map operation constructor.
        :mapper: mapper function.
        :data: list of rows to map.
        """
        self.mapper = mapper
        self.data = None

    def run(self):
        for string in self.data:
            for map_result in self.mapper(string):
                yield map_result


class Sort(object):
    """Sort operation."""
    def __init__(self, column_ids, reverse=True):
        """
        Sort operation constructor.
        :column_ids: list of column names to sort by.
        :data: data to sort.
        :reverse: (optional) when True, sort in descending order.
        """
        self.column_ids = column_ids
        self.data = None
        self.reverse = reverse

    def run(self):
        if (len(self.data) > 0 and
           not(set(self.column_ids).issubset(set(self.data[0])))):
            raise ValueError('Wrong keys for sort')
        self.data.sort(key=operator.itemgetter(*self.column_ids),
                       reverse=self.reverse)


class Fold(object):
    """Fold operation."""
    def __init__(self, folder, init_state):
        """
        Fold operation constructor.
        :folder: folder function.
        :data: data to fold.
        :init_state: initial state for folder.
        """
        self.folder = folder
        self.init_state = init_state
        self.data = None

    def run(self):
        cur_state = self.init_state
        for string in self.data:
            cur_state = self.folder(cur_state, string)
        return cur_state


class Reduce(object):
    """Reduce operation."""
    def __init__(self, reducer, column_ids):
        """
        Reduce operation constructor.
        :reducer: reducer function.
        :column_ids: list of column names to reduce by.
        :data: data to reduce.
        """
        self.reducer = reducer
        self.column_ids = column_ids
        self.data = None

    def run(self):
        for batch in self.get_batches():
            for reduce_res in self.reducer(batch):
                yield reduce_res

    def get_batches(self):
        """
        Sort self.data by self.column_ids.
        Split the data into pieces with the same key values, keys are
        self.column_ids. Return these pieces as generator.
        """
        if (len(self.data) > 0 and
           not(set(self.column_ids).issubset(set(self.data[0])))):
            raise ValueError('Wrong key for reduce')
        self.data.sort(key=operator.itemgetter(*self.column_ids))
        for k, g in groupby(self.data,
                            key=operator.itemgetter(*self.column_ids)):
            yield list(g)


class Join(object):
    """Join operation."""
    def __init__(self, input_object, column_ids=[], strategy='inner'):
        """
        Join operation constructor.
        :input_object: ComputeGraph object, later we will take its table as
        data to join.
        :possible_inputs: dict of inputs, that were calculated during previous
        stages.
        :column_ids: list of column names to join by. Can be empty only when
        outer strategy is used.
        :strategy: (optional) can be «inner», «left», «right» or «outer».
        """
        self.input_object = input_object
        self.possible_inputs = {}
        self.column_ids = column_ids
        self.strategy = strategy
        self.data = None

    def run(self):
        self.to_join = self.possible_inputs[self.input_object]

        if len(self.column_ids) == 0:
            if self.strategy == 'outer':
                for l_record, r_record in product(self.data, self.to_join):
                    yield {**l_record, **r_record}
                return
            else:
                raise ValueError('Wrong key for join')
        if len(self.to_join) == 0 or len(self.data) == 0:
            raise ValueError('Cant join empty data')
        if not(set(self.column_ids).issubset(set(self.to_join[0]))):
            raise ValueError('Wrong key for join')
        if not(set(self.column_ids).issubset(set(self.data[0]))):
            raise ValueError('Wrong key for join')

        only_rhs_keys = set(self.to_join[0].keys()) - set(self.column_ids)
        only_lhs_keys = set(self.data[0].keys()) - set(self.column_ids)
        rhs_nan_dict = {key: None for key in only_rhs_keys}
        lhs_nan_dict = {key: None for key in only_lhs_keys}

        lhs_generator = self.get_batches(self.data)
        rhs_generator = self.get_batches(self.to_join)
        lhs = next(lhs_generator, None)
        rhs = next(rhs_generator, None)

        getter = operator.itemgetter(*self.column_ids)
        while (lhs is not None) and (rhs is not None):
            if getter(lhs[0]) == getter(rhs[0]):
                for l_record, r_record in product(lhs, rhs):
                    yield {**l_record, **r_record}
                lhs = next(lhs_generator, None)
                rhs = next(rhs_generator, None)
                continue
            if getter(lhs[0]) < getter(rhs[0]):
                if self.strategy in ['left', 'outer']:
                    for l_record in lhs:
                        yield {**l_record, **rhs_nan_dict}
                lhs = next(lhs_generator, None)
                continue
            if getter(lhs[0]) > getter(rhs[0]):
                if self.strategy in ['right', 'outer']:
                    for r_record in rhs:
                        yield {**r_record, **lhs_nan_dict}
                rhs = next(rhs_generator, None)
                continue
        if self.strategy in ['left', 'outer']:
            while lhs is not None:
                for l_record in lhs:
                    yield {**l_record, **rhs_nan_dict}
                lhs = next(lhs_generator, None)
        if self.strategy in ['right', 'outer']:
            while rhs is not None:
                for r_record in rhs:
                    yield {**r_record, **lhs_nan_dict}
                rhs = next(rhs_generator, None)

    def get_batches(self, data):
        """
        Sort data by self.column_ids.
        Split data into pieces with the same key values, keys are
        self.column_ids. Return these pieces as generator.
        """
        data.sort(key=operator.itemgetter(*self.column_ids))
        for k, g in groupby(data, key=operator.itemgetter(*self.column_ids)):
            yield list(g)
